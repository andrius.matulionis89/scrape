import requests

TARGET = f"https://www.ebay.com/sch/i.html?_nkw={0}&LH_TitleDesc=0&_odkw={0}"


def request_web_source(url: str) -> str:
    """Takes page source from URL as string.

    :param url: url of page
    :return: page source
    """
    res = requests.get(url)
    return str(res.content.decode("utf-8"))

