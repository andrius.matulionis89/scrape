import requests
from bs4 import BeautifulSoup
from product import Product


url = 'https://www.ebay.com/sch/i.html?_nkw='


def extract(asin):
    data = requests.get(url + str(asin)).text
    soup = BeautifulSoup(data, 'html.parser')
    product_block = soup.find_all('div', class_='s-item__info clearfix')
    return product_block


def main():
    items = extract("lego")
    for item in items:
        product_info = Product(item).get_db_table()
        product_info.save()


if __name__ == "__main__":
    main()


#bile kad kazka parasyt