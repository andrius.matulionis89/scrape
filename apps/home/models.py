# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.db import models


class Scrape(models.Model):

    #nr = models.CharField(max_length=20,primary_key=True)
    product = models.CharField(max_length=100)
    price = models.FloatField(default=0.0)
    location = models.CharField(max_length=100)
    quality = models.CharField(max_length=50)
    seller = models.CharField(max_length=50)
    url = models.CharField(max_length=250)



class Meta:
    db_table = 'scrape'



