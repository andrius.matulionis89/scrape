from pathlib import Path
from scraper.data_collecting import request_web_source

project_root = Path(__file__).parents[1]


def test_request_web_source():
    url = "https://www.example.com"
    output = request_web_source(url)
    with open(project_root / 'tests/example_web.txt', 'r', encoding="utf-8") as my_file:
        ebay_source = my_file.read()
    assert output == ebay_source
